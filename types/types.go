package types

// ServiceCollection holds the registered services ready to be used
type ServiceCollection interface {
	// Get resolves the interface into the reference parameter `i`.
	//
	// Pass the pointer to a variable that will hold the interface.
	Get(i interface{}) error
}

// ServiceFactory provides the logic to initialize a service
//
// Be wary of circular dependency when resolving a service from `sc`
type ServiceFactory func(sc ServiceCollection) (interface{}, error)

// ServiceCollectionBuilder provides functions to build a `ServiceCollection`.
type ServiceCollectionBuilder interface {
	// SingletonConcrete registers a pre-initialized interface as a singleton
	SingletonConcrete(i interface{})

	// SingletonFactory registers a factory function to lazy-init an interface as a singleton.
	//
	// Pass an empty variable of the interface type as `i`.
	SingletonFactory(i interface{}, f ServiceFactory)

	// TransientFactory registers a factory function that is called everytime the interface is
	// requested.
	//
	// Pass an empty variable of the interface type as `i`.
	TransientFactory(i interface{}, f ServiceFactory)
}
